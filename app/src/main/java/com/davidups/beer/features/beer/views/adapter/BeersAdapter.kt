package com.davidups.beer.features.beer.views.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.davidups.beer.R
import com.davidups.beer.core.extensions.inflate
import com.davidups.beer.core.extensions.loadFromUrl
import com.davidups.beer.features.beer.data.models.view.BeerView
import kotlinx.android.synthetic.main.beer_item.view.*
import kotlin.properties.Delegates

class BeersAdapter : RecyclerView.Adapter<BeersAdapter.EnvironmentHolder>() {

    internal var collection: List<BeerView> by Delegates.observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }
    internal var beerListener: (BeerView) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        EnvironmentHolder(parent.inflate(R.layout.beer_item))

    override fun getItemCount(): Int = collection.size

    override fun onBindViewHolder(holder: EnvironmentHolder, position: Int) {
        holder.bind(collection[position], beerListener)
    }

    inner class EnvironmentHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: BeerView, beerListener: (BeerView) -> Unit) {
            itemView.ivBeer.loadFromUrl(item.image_url)
            itemView.tvBeerInfo.text = item.name
            itemView.cvItem.setOnClickListener { beerListener(item) }
            itemView.btnMoreInfo.setOnClickListener { beerListener(item) }
        }
    }
}
