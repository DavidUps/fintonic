package com.davidups.beer.features.beer.data.models.data

import com.davidups.beer.core.extensions.empty
import com.davidups.beer.core.extensions.orEmpty
import com.davidups.beer.features.beer.data.models.entity.BeerEntity
import com.davidups.beer.features.beer.data.models.view.BeerView

data class Beer(
    private val id: Int?,
    private val name: String?,
    private val description: String?,
    private val image_url: String?,
    private val abv: Double?
) {

    companion object {
        fun empty() = Beer(Int.empty(), String.empty(), String.empty(), String.empty(), Double.empty())
    }

    fun toView() = BeerView(
        name.orEmpty(),
        description.orEmpty(),
        image_url.orEmpty(),
        abv.orEmpty()
    )

    fun toEntity() = BeerEntity(id, name, description, image_url, abv)
}