package com.davidups.beer.core.platform

import android.content.Context
import com.davidups.beer.core.extensions.networkInfo


class NetworkHandler
(private val context: Context) {
    val isConnected get() = context.networkInfo?.isConnected
}