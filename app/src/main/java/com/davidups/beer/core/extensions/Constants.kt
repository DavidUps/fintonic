package com.davidups.beer.core.extensions

class Constants {

    companion object {
        val LOG_ERROR_TAG = "Error"
        const val BEER = "beer"
    }
}