package com.davidups.beer.core.functional

import com.davidups.beer.core.exception.Failure
import com.davidups.beer.core.extensions.empty

sealed class State<out T : Any>

class Success<out T : Any>(val data: T) : State<T>()

class Error(
    val failure: Failure
) : State<Nothing>()

fun error() = Error(Failure.ServerError(Int.empty()))
fun error(code:Int) = Error(Failure.ServerError(code))